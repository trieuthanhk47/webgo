module.exports = {
    /**************************** SAN PHAM 1 ****************************/

    SAN_PHAM1: [




        {
            anh: './sp/wood.png',
            kieu: 'sp',

            ten: ' Eucalyptus core, Grade A veneer glue core with good price',
            gia: '$171.00 - $245.00 Cubic Meter',
            giacu: 'Giá:  $174.00 - $248.00 Cubic Meter',
            noidung: 'Supply peel off glue, eucalyptus for export. Supply wood peeler board...',
            link: 'Eucalyptus-Core-Veneer-Grade-A-the-good-price',

        },

        {
            anh: './sp/piece-of-wood.png',
            kieu: 'sp',

            ten: 'Acacia sawn timber/lumber/ for pallets or fiurnture',
            gia: '$157.00 - $267.00/ Cubic Meter',
            giacu: '$159.00 - $269.00 Cubic Meter',
            noidung: 'Currently, we supply sawn timber (including raw lumber and dried lumber) to the export market with diverse product designs and rich specifications...',



            link: 'Acacia-sawn-timber-lumber-for-pallets-or-furniture',

        },

        {
            anh: './sp/milled-wood.png',
            kieu: 'sp',

            ten: 'Excellent Quality Wood chips - Very Competitive Wood chips from Vietnam - Pine/ Acacia...',
            gia: '$16.00 - $30.00 / Square Meter',
            giacu: '$19.00 - $35.00 / Square Meter',
            noidung: 'We provide wood chips are wood chips, wood chips. an important raw material in the combustion industry and some renewable fuel industries.',

            link: 'Excellent-Quality-Wood-chips',

        },

        {
            anh: './sp/pallet .png',
            kieu: 'sp',

            ten: ' Wholesale New Epal/ Euro Wood Pallets/ Pine Wood pallet.',
            gia: '$12.00 - $20.00/ Piece',
            giacu: '$18.00 - $25.00/ Piece',
            noidung: 'We provide, export PALLET with the cheapest price to countries around the world...',

            link: 'Wholesale-New-Epal-Euro-Wood-Pallets-Pine-Wood-pallet',

        },
 {
            anh: './ginger/Ginger-Fresh-Best-Quality-In-Carton-Professional-Export-Fresh-Ginger3.jpg',
            kieu: 'sp',

            ten: ' Ginger Fresh Best Quality In Carton Professional Export Fresh Ginger.',
            gia: '$1.80 - $3.20/ Kilogram',
            giacu: '$1.90 - $3.50/ Kilogram',
            noidung: 'We provide, export PALLET with the cheapest price to countries around the world...',

            link: 'Ginger-Fresh-Best-Quality-In-Carton-Professional-Export-Fresh-Ginger',

        },









    ],

    SAN_PHAM2: [
        /**************************** kệ máy giặt ****************************/



        {
            anh: './kemaygiat/ke-may-giat1.png',
            kieu: 'kemaygiat',

            ten: 'KỆ MÁY GIẶT MÃ MG01 ',
            gia: 'Giá:1.750.000 vnđ',
            giacu: 'Giá: 1.980.000 vnđ',


            link: 'ke-may-giat',

        },
        {
            anh: './kemaygiat/ke-may-giat2.png',
            kieu: 'kemaygiat',

            ten: 'KỆ MÁY GIẶT MÃ MG02',
            gia: 'Giá:1.250.000 vnđ',
            giacu: 'Giá: 1.650.000  vnđ',


            link: 'ke-may-giat-ma-mg02',

        },




        {
            anh: './kemaygiat/ke-may-giat3.png',
            kieu: 'kemaygiat',

            ten: 'KỆ MÁY GIẶT MÃ MG03',
            gia: 'Giá: 2.700.000 vnđ',
            giacu: 'Giá: 2.950.000 vnđ',


            link: 'ke-may-giat-ma-mg03',

        },

        {
            anh: './kemaygiat/ke-may-giat4.png',
            kieu: 'kemaygiat',

            ten: 'KỆ MÁY GIẶT MÃ MG04',
            gia: 'Giá: 2.700.000 vnđ',
            giacu: 'Giá: 2.950.000 vnđ',


            link: 'ke-may-giat-ma-mg04',

        },

        {
            anh: './kemaygiat/kvs6.png',
            kieu: 'kemaygiat',

            ten: 'KỆ ĐA NĂNG TRONG NHÀ TẮM MÃ NT01',
            gia: 'Giá: 820.000 vnđ',
            giacu: 'Giá: 990.000 vnđ',


            link: 'ke-da-dang-trong-nha-tam-ma-nt01',

        },

        {
            anh: './kemaygiat/kvss1.png',
            kieu: 'kemaygiat',

            ten: 'KỆ ĐA NĂNG TRONG NHÀ TẮM MÃ NT02',
            gia: 'Giá: 1.250.000 vnđ',
            giacu: 'Giá: 1.570.000 vnđ',


            link: 'ke-da-dang-trong-nha-tam-ma-nt02',

        },
        {
            anh: './kemaygiat/kmg1.png',
            kieu: 'kemaygiat',

            ten: 'Kệ để máy giặt cửa ngang 2 ngăn',
            gia: 'Giá: 1.597.000 vnđ',
            giacu: 'Giá: 1.970.000 vnđ',


            link: 'ke-de-vat-dung-tren-may-giat-2-ngan',

        },
        {
            anh: './kemaygiat/kmgg1.png',
            kieu: 'kemaygiat',

            ten: 'Kệ Máy Giặt KMG 1004 ',
            gia: 'Giá: 1.897.000 vnđ',
            giacu: 'Giá: 2.199.000 vnđ',


            link: 'ke-may-giat-may-say-da-nang-kmg-1004-sat-hop',

        },

    ],















    SAN_PHAM3: [

        /**************************** kệ sách ****************************/


        {
            anh: './kesach/ke-sach1.png',
            kieu: 'kesach',

            ten: 'GIÁ, KỆ SÁCH 5 TẦNG',
            gia: 'Giá:1.660.000 vnđ',
            giacu: 'Giá: 1.800.000 vnđ',


            link: 'ke-sach1',

        },
        {
            anh: './kesach/ke-sach2.png',
            kieu: 'kesach',

            ten: 'GIÁ, KỆ SÁCH 4 TẦNG ',
            gia: 'Giá: 2.250.000 vnđ',
            giacu: 'Giá: 2.650.000 vnđ',


            link: 'ke-sach2',

        },




        {
            anh: './kesach/ke-sach3.png',
            kieu: 'kesach',

            ten: 'GIÁ, KỆ SÁCH ',
            gia: 'Giá: 450.000 - 1.600.000  vnđ',



            link: 'ke-sach3',

        },

        {
            anh: './kesach/ke-sach4.png',
            kieu: 'kesach',

            ten: 'GIÁ, KỆ SÁCH 3 TẦNG',
            gia: 'Giá:450.000 vnđ',
            giacu: 'Giá: 600.000 vnđ',


            link: 'ke-sach4',

        },
        {
            anh: './kesach/ks1.png',
            kieu: 'kesach',

            ten: 'Kệ tủ quần áo tân cổ điển ',
            gia: 'Giá: 3.100.000 vnđ',
            giacu: 'Giá: 3.499.000 vnđ',


            link: 'ke-tu-quan-ao',

        },

        {
            anh: './kesach/kss1.png',
            kieu: 'kesach',

            ten: 'Kệ Tủ Quần Áo mã KQA 01',
            gia: 'Giá: 2.990.000 vnđ',
            giacu: 'Giá: 3.199.000 vnđ',


            link: 'ke-tu-quan-ao-kqa',

        },

        {
            anh: './kesach/ksss1.png',
            kieu: 'kesach',

            ten: 'Kệ Để Đồ Đa Năng KN 153',
            gia: 'Giá: 1.890.000 vnđ',
            giacu: 'Giá: 2.199.000 vnđ',


            link: 'ke-de-do-da-nang-kn153',

        },
        {
            anh: './kesach/kssss1.png',
            kieu: 'kesach',

            ten: 'Kệ để máy in, tài liệu sách vở ',
            gia: 'Giá: 1.190.000 vnđ',
            giacu: 'Giá: 1.390.000 vnđ',


            link: 'ke-de-may-in-tai-lieu-sach-vo',

        },

    ],







    SAN_PHAM4: [

        /**************************** kệ hoa ****************************/


        {
            anh: './kehoa/ke-hoa1.png',
            kieu: 'kehoa',

            ten: 'KỆ HOA SẮT NGHỆ THUẬT CHỮ Z',
            gia: 'Giá:397.000 vnđ',
            giacu: 'Giá: 500.000 vnđ',


            link: 'ke-hoa1',

        },
        {
            anh: './kehoa/ke-hoa2.png',
            kieu: 'kehoa',

            ten: 'KỆ HOA NGHỆ THUẬT BẬC THANG ',
            gia: 'Giá:399.000  -  580.000 vnđ',



            link: 'ke-hoa2',

        },




        {
            anh: './kehoa/ke-hoa3.png',
            kieu: 'kehoa',

            ten: 'KỆ HOA SẮT NGHỆ THUẬT KH02 ',
            gia: 'Giá: 1.200.000 vnđ',
            giacu: 'Giá: 1.800.000 vnđ',


            link: 'ke-hoa-nghe-thuat-ma-kh03',

        },

        {
            anh: './kehoa/ke-hoa4.png',
            kieu: 'kehoa',

            ten: 'KỆ HOA SẮT NGHỆ THUẬT MÃ KH03',
            gia: 'Giá:1.100.000 vnđ',
            giacu: 'Giá: 1.300.000 vnđ',


            link: 'ke-hoa4',

        },
        {
            anh: './kehoa/68.png',
            kieu: 'kehoa',

            ten: 'Kệ Trang Trí Cây Xanh KG14A',
            gia: 'Giá:1.110.000 vnđ',
            giacu: 'Giá: 1.500.000 vnđ',


            link: 'ke-hoa5',

        },
        {
            anh: './kehoa/28.png',
            kieu: 'kehoa',

            ten: 'KỆ HOA SẮT NGHỆ THUẬT MÃ KH05',
            gia: 'Giá: 487.000 vnđ',
            giacu: 'Giá: 600.000 vnđ',


            link: 'ke-hoa6',

        },
        {
            anh: './kehoa/52.png',
            kieu: 'kehoa',

            ten: 'KỆ HOA, kệ sách đa năng',
            gia: 'Giá: LIÊN HỆ',



            link: 'ke-hoa6',

        },

        {
            anh: './kehoa/555.png',
            kieu: 'kehoa',

            ten: 'Kệ Trang Trí Cây Xanh',
            gia: 'Giá: 497.000 vnđ',
            giacu: 'Giá: 700.000 vnđ',



            link: 'ke-hoa7',

        },






    ],





    SAN_PHAM5: [

        /**************************** tin tức ****************************/


        {
            anh: './tintuc/sofacafe3.png',
            kieu: 'tintuc',
            ten: 'Những lưu ý không thể bỏ qua khi bạn muốn mua sofa giá rẻ',
            link: 'nhung-luu-y-khong-the-bo-qua-khi-ban-muon-mua-sofa-gia-re',

        },


        {
            anh: './tintuc/sofa-karaoke.png',
            kieu: 'tintuc',

            ten: 'Chọn Sofa cho phòng Karaoke sao cho đúng cách',
            gia: 'Giá:600.000 vnđ',
            giacu: 'Giá: 800.000 vnđ',


            link: 'chon-sofa-cho-phong-karaoke-sao-cho-dung-cach',

        },




    ],




    SAN_PHAM6: [

        /**************************** trang ****************************/


        {
            anh: './tranh/1.png',
            kieu: 'tintuc',
            ten: 'TRANH CANVAS MÃ TC01',
            link: 'tranh-canvas-ma01',
            gia: 'Giá: Liên Hệ',

        },

        {
            anh: './tranh/2.png',
            kieu: 'tintuc',
            ten: 'TRANH CANVAS MÃ TC02',
            link: 'tranh-canvas-ma02',
            gia: 'Giá: Liên Hệ',

        },

        {
            anh: './tranh/3.png',
            kieu: 'tintuc',
            ten: 'TRANH CANVAS MÃ TC03',
            link: 'tranh-canvas-ma03',
            gia: 'Giá: Liên Hệ',

        },

        {
            anh: './tranh/4.png',
            kieu: 'tintuc',
            ten: 'TRANH CANVAS MÃ TC04',
            link: 'tranh-canvas-ma04',
            gia: 'Giá: Liên Hệ',

        },


    ],





    SAN_PHAM7: [
        /**************************** tat sản phẩm kệ máy giặt ****************************/

        {
            anh: './sp/ke-bep1.png',
            kieu: 'sp',

            ten: ' KỆ BẾP 4 TẦNG MÃ KB01',
            gia: 'Giá:600.000 vnđ',
            giacu: 'Giá: 800.000 vnđ',

            link: 'ke-bep-6tang-ma-kb01',

        },

        {
            anh: './sp/ke-bep2.png',
            kieu: 'sp',

            ten: 'KỆ BẾP 4 TẦNG MÃ KB02',
            gia: 'Giá:1.600.000 vnđ',
            giacu: 'Giá: 1.250.000 vnđ',


            link: 'ke-bep-4tang-ma-kb02',

        },

        {
            anh: './sp/ke-bep3.png',
            kieu: 'sp',

            ten: 'KỆ ĐỂ LÒ VI SÓNG, NỒI CƠM ĐIỆN Mã KB03',
            gia: 'Giá: 1.096.000  vnđ',
            giacu: 'Giá:910.000 vnđ',


            link: 'ke-de-lo-vi-song-noi-com-dien-lo-nuong',

        },

        {
            anh: './sp/ke-bep4.png',
            kieu: 'sp',

            ten: ' KỆ GÓC TƯỜNG ',
            gia: 'Giá:600.000 vnđ',
            giacu: 'Giá: 800.000 vnđ',


            link: 'ke-goc-tuong',

        },




        {
            anh: './sp/ke-bep5.png',
            kieu: 'sp',

            ten: 'KỆ LO VI SÓNG NHÀ BẾP MÃ KB04 ',
            gia: 'Giá:600.000 vnđ',
            giacu: 'Giá: 800.000 vnđ',


            link: 'ke-lo-vi-song-nha-bep-ma-kb04',

        },



        {
            anh: './sp/ke-bep6.png',
            kieu: 'sp',

            ten: 'Kệ Để Lò Vi Sóng 5 Tầng ',
            gia: 'Giá:1.300.000 vnđ',
            giacu: 'Giá: 1.500.000 vnđ',


            link: 'ke-de-lo-vi-song-5-tang',

        },



        {
            anh: './sp/ke-bep7.png',
            kieu: 'sp',

            ten: 'KỆ LÒ VI SÓNG MÃ KB05',
            gia: 'Giá:600.000 vnđ',
            giacu: 'Giá: 800.000 vnđ',


            link: 'ke-lo-vi-song',

        },

        {
            anh: './sp/ke-bep8.png',
            kieu: 'sp',

            ten: 'KỆ LO VI SÓNG NHÀ BẾP MÃ KB06',
            gia: 'Giá:600.000 vnđ',
            giacu: 'Giá: 800.000 vnđ',


            link: 'ke-lo-vi-song-nha-bep-ma-kb06',

        },





















    ],













}