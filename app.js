var express = require('express');
var app = express();
var morgan = require("morgan");
var bodyParser = require('body-parser');
var path = require("path")
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); // get information from html forms
var port = process.env.PORT || 801;
var gmail = require('./guimail')
app.use(morgan('dev')); // log every request to the console

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
var danhsachsp = require('./dulieu/danhsachsanpham')


app.get("/", function(req, res) {
    res.render('index', {
        isactive: 1,
            sanpham1: danhsachsp.SAN_PHAM1,
        sanpham2: danhsachsp.SAN_PHAM2,
        sanpham3: danhsachsp.SAN_PHAM3,
        sanpham4: danhsachsp.SAN_PHAM4,
            sanpham5: danhsachsp.SAN_PHAM5,
            sanpham6: danhsachsp.SAN_PHAM6,
             sanpham7: danhsachsp.SAN_PHAM7,
        sanpham8: danhsachsp.SAN_PHAM8,
        sanpham9: danhsachsp.SAN_PHAM9,
            sanpham10: danhsachsp.SAN_PHAM10,
            sanpham11: danhsachsp.SAN_PHAM11,
             sanpham12: danhsachsp.SAN_PHAM12,
        sanpham13: danhsachsp.SAN_PHAM13,
        sanpham14: danhsachsp.SAN_PHAM14,
            sanpham15: danhsachsp.SAN_PHAM15,
            sanpham16: danhsachsp.SAN_PHAM16,

    });
});
app.post('/', function(req, res){
    let ten = req.body.tenkhachhang
    let sdt = req.body.didong
    let mail = req.body.emailkhach
    let nd = req.body.noidung
    gmail.guimaildi('trieuthanhk47@gmail.com', //nguoi nhan
        ten+' yêu cậu nhận thông tin dự án', //tieu de cua email 
        'Tên khách hàng: '+ten+'<br>SDT:'+sdt +'<br>Email:'+mail+'<br>Nội dung: '+nd
        )
    res.send("Chúng tôi đã nhận yêu cầu")
})
app.get("/Eucalyptus-Core-Veneer-Grade-A-the-good-price", function(req, res) {
    res.render('Eucalyptus-Core-Veneer-Grade-A-the-good-price', {
        isactive: 1,

              sanpham1: danhsachsp.SAN_PHAM1,
        sanpham2: danhsachsp.SAN_PHAM2,
        sanpham3: danhsachsp.SAN_PHAM3,
        sanpham4: danhsachsp.SAN_PHAM4,
            sanpham5: danhsachsp.SAN_PHAM5,
            sanpham6: danhsachsp.SAN_PHAM6,
             sanpham7: danhsachsp.SAN_PHAM7,
        sanpham8: danhsachsp.SAN_PHAM8,
        sanpham9: danhsachsp.SAN_PHAM9,
            sanpham10: danhsachsp.SAN_PHAM10,
            sanpham11: danhsachsp.SAN_PHAM11,
             sanpham12: danhsachsp.SAN_PHAM12,
        sanpham13: danhsachsp.SAN_PHAM13,
        sanpham14: danhsachsp.SAN_PHAM14,
            sanpham15: danhsachsp.SAN_PHAM15,
            sanpham16: danhsachsp.SAN_PHAM16,

    });
});

app.get("/Acacia-sawn-timber-lumber-for-pallets-or-furniture", function(req, res) {
    res.render('Acacia-sawn-timber-lumber-for-pallets-or-furniture', {
        isactive: 1,
            sanpham1: danhsachsp.SAN_PHAM1,
        sanpham2: danhsachsp.SAN_PHAM2,
        sanpham3: danhsachsp.SAN_PHAM3,
        sanpham4: danhsachsp.SAN_PHAM4,
            sanpham5: danhsachsp.SAN_PHAM5,
            sanpham6: danhsachsp.SAN_PHAM6,
             sanpham7: danhsachsp.SAN_PHAM7,
        sanpham8: danhsachsp.SAN_PHAM8,
        sanpham9: danhsachsp.SAN_PHAM9,
            sanpham10: danhsachsp.SAN_PHAM10,
            sanpham11: danhsachsp.SAN_PHAM11,
             sanpham12: danhsachsp.SAN_PHAM12,
        sanpham13: danhsachsp.SAN_PHAM13,
        sanpham14: danhsachsp.SAN_PHAM14,
            sanpham15: danhsachsp.SAN_PHAM15,
            sanpham16: danhsachsp.SAN_PHAM16,
    });
});

app.get("/Excellent-Quality-Wood-chips", function(req, res) {
    res.render('Excellent-Quality-Wood-chips', {
        isactive: 1,
            sanpham1: danhsachsp.SAN_PHAM1,
        sanpham2: danhsachsp.SAN_PHAM2,
        sanpham3: danhsachsp.SAN_PHAM3,
        sanpham4: danhsachsp.SAN_PHAM4,
            sanpham5: danhsachsp.SAN_PHAM5,
            sanpham6: danhsachsp.SAN_PHAM6,
             sanpham7: danhsachsp.SAN_PHAM7,
        sanpham8: danhsachsp.SAN_PHAM8,
        sanpham9: danhsachsp.SAN_PHAM9,
            sanpham10: danhsachsp.SAN_PHAM10,
            sanpham11: danhsachsp.SAN_PHAM11,
             sanpham12: danhsachsp.SAN_PHAM12,
        sanpham13: danhsachsp.SAN_PHAM13,
        sanpham14: danhsachsp.SAN_PHAM14,
            sanpham15: danhsachsp.SAN_PHAM15,
            sanpham16: danhsachsp.SAN_PHAM16,
    });
});

app.get("/Wholesale-New-Epal-Euro-Wood-Pallets-Pine-Wood-pallet", function(req, res) {
    res.render('Wholesale-New-Epal-Euro-Wood-Pallets-Pine-Wood-pallet', {
        isactive: 1,
            sanpham1: danhsachsp.SAN_PHAM1,
        sanpham2: danhsachsp.SAN_PHAM2,
        sanpham3: danhsachsp.SAN_PHAM3,
        sanpham4: danhsachsp.SAN_PHAM4,
            sanpham5: danhsachsp.SAN_PHAM5,
            sanpham6: danhsachsp.SAN_PHAM6,
             sanpham7: danhsachsp.SAN_PHAM7,
        sanpham8: danhsachsp.SAN_PHAM8,
        sanpham9: danhsachsp.SAN_PHAM9,
            sanpham10: danhsachsp.SAN_PHAM10,
            sanpham11: danhsachsp.SAN_PHAM11,
             sanpham12: danhsachsp.SAN_PHAM12,
        sanpham13: danhsachsp.SAN_PHAM13,
        sanpham14: danhsachsp.SAN_PHAM14,
            sanpham15: danhsachsp.SAN_PHAM15,
            sanpham16: danhsachsp.SAN_PHAM16,
    });
});

app.get("/Ginger-Fresh-Best-Quality-In-Carton-Professional-Export-Fresh-Ginger", function(req, res) {
    res.render('Ginger-Fresh-Best-Quality-In-Carton-Professional-Export-Fresh-Ginger', {
        isactive: 1,
            sanpham1: danhsachsp.SAN_PHAM1,
        sanpham2: danhsachsp.SAN_PHAM2,
        sanpham3: danhsachsp.SAN_PHAM3,
        sanpham4: danhsachsp.SAN_PHAM4,
            sanpham5: danhsachsp.SAN_PHAM5,
            sanpham6: danhsachsp.SAN_PHAM6,
             sanpham7: danhsachsp.SAN_PHAM7,
        sanpham8: danhsachsp.SAN_PHAM8,
        sanpham9: danhsachsp.SAN_PHAM9,
            sanpham10: danhsachsp.SAN_PHAM10,
            sanpham11: danhsachsp.SAN_PHAM11,
             sanpham12: danhsachsp.SAN_PHAM12,
        sanpham13: danhsachsp.SAN_PHAM13,
        sanpham14: danhsachsp.SAN_PHAM14,
            sanpham15: danhsachsp.SAN_PHAM15,
            sanpham16: danhsachsp.SAN_PHAM16,
    });
});



app.get("/lien-he", function(req, res) {
    res.render('lien-he', {
        isactive: 1, sanpham1: danhsachsp.SAN_PHAM1,
        sanpham2: danhsachsp.SAN_PHAM2,
        sanpham3: danhsachsp.SAN_PHAM3,
        sanpham4: danhsachsp.SAN_PHAM4,
            sanpham5: danhsachsp.SAN_PHAM5,
            sanpham6: danhsachsp.SAN_PHAM6,
             sanpham7: danhsachsp.SAN_PHAM7,
        sanpham8: danhsachsp.SAN_PHAM8,
        sanpham9: danhsachsp.SAN_PHAM9,
            sanpham10: danhsachsp.SAN_PHAM10,
            sanpham11: danhsachsp.SAN_PHAM11,
             sanpham12: danhsachsp.SAN_PHAM12,
        sanpham13: danhsachsp.SAN_PHAM13,
        sanpham14: danhsachsp.SAN_PHAM14,
            sanpham15: danhsachsp.SAN_PHAM15,
            sanpham16: danhsachsp.SAN_PHAM16,
    });
});









app.listen(port);
console.log('The magic happens on port ' + port);